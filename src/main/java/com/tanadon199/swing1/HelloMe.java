/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tanadon199.swing1;
import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JTextField;

class MyActionListener implements ActionListener {

    @Override
    public void actionPerformed(ActionEvent e) {
        System.out.println("MyActionListener: Action");
    }

}

/**
 *
 * @author Kitty
 */
public class HelloMe implements ActionListener{
    public static void main(String[] args) {
        JFrame framemain = new JFrame("Hello Me");
        framemain.setSize(500, 300);
        framemain.setLayout(null);
        framemain.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        
        
        JLabel lbname = new JLabel("Your name: ");
        lbname.setSize(80, 20);
        lbname.setLocation(5,5);
        lbname.setBackground(Color.orange);
        lbname.setOpaque(true);
        
        
        JTextField tname = new JTextField();
        tname.setSize(200, 20);
        tname.setLocation(90, 5);
        
        JLabel lbHello = new JLabel("Hello...",JLabel.CENTER);
        lbHello.setSize( 200, 20);
        lbHello.setLocation(90, 70);
        lbHello.setBackground(Color.WHITE);
        lbHello.setOpaque(true);
        
        JButton btHello = new JButton("Hello");
        btHello.setSize(80, 20);
        btHello.setLocation(90, 40);
        
        MyActionListener action =  new MyActionListener();
        btHello.addActionListener(action);
        btHello.addActionListener(new HelloMe());
        btHello.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                String name = tname.getText();
                lbHello.setText("Hello "+name);
                
            }
        });
               
        
        ActionListener actionListener = new ActionListener(){ // Anonymous Class เดิมทีเป็น interface
            @Override
            public void actionPerformed(ActionEvent e) {
                System.out.println("Anonymous Class: Action");
            }
            
        };
        btHello.addActionListener(actionListener);
        
        
        framemain.add(lbname);
        framemain.add(tname);
        framemain.add(btHello);
        framemain.add(lbHello);
        
        framemain.setVisible(true);
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        System.out.println("HelloME: Action");
    }
}

