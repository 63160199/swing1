/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tanadon199.swing1;

import java.awt.Color;
import java.awt.Font;
import javax.swing.JFrame;
import javax.swing.JLabel;

/**
 *
 * @author Kitty
 */
public class Frame1 {
    public static void main(String[] args) {
        JFrame frame = new JFrame ();
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setSize(500, 300);
        JLabel lb = new JLabel("Hello World!",JLabel.CENTER);
        frame.add(lb);
        lb.setBackground(Color.yellow);
        lb.setFont(new Font("Vernada",Font.PLAIN,70));
        lb.setOpaque(true); 
        
        
        frame.setVisible(true);
    }
}
